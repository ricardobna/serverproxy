import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ProxyThread extends Thread{
	    private Socket cliente;
	    private Socket destino;
	    private final String enderecoDoDestino;
	    private final int portaDoDestino;
	    private String requisicaoInicialDoCliente;
	    
	    ProxyThread(Socket clienteProxy, String enderecoDoDestino, String requisicaoInicialDoCliente,int portaDoDestino) {
	        this.enderecoDoDestino = enderecoDoDestino;
	        this.portaDoDestino = portaDoDestino;
	        this.cliente = clienteProxy;
	        this.requisicaoInicialDoCliente = requisicaoInicialDoCliente;
	        this.start();
	    }

	    @Override
	    public void run() {
	        try {
	            final PrintWriter outputDoCliente = new PrintWriter(cliente.getOutputStream());
	            final BufferedReader inputDoCliente = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
	            try {
	                destino = new Socket(enderecoDoDestino, portaDoDestino);
	            } catch (IOException e) {
	                throw new RuntimeException(e);
	            }
	            final BufferedReader inputDoDestino = new BufferedReader(new InputStreamReader(destino.getInputStream()));
	            final PrintWriter outputDoDestino = new PrintWriter(destino.getOutputStream(), true);
                outputDoDestino.println(requisicaoInicialDoCliente);
                final String endLine = "\r\n";
                
	            String stringParcialRecebidaDoDestino;
	            StringBuffer stringCompletaRecebidaDoDestino = new StringBuffer();
	            try {
	                while ((stringParcialRecebidaDoDestino = inputDoDestino.readLine()) != null) {
	                	if(stringParcialRecebidaDoDestino.equals("")) break;
						stringCompletaRecebidaDoDestino.append(stringParcialRecebidaDoDestino);
	                	stringCompletaRecebidaDoDestino.append(endLine);
	                }
	                stringCompletaRecebidaDoDestino.append(endLine);
					System.out.println(stringCompletaRecebidaDoDestino);
	                outputDoCliente.println(stringCompletaRecebidaDoDestino);
	            } catch (IOException e) {
	                e.printStackTrace();
	            } finally {
	                try {
	                    if (destino != null) {
	                    	destino.close();
	                    }
	                    if(cliente != null) {
	                    	cliente.close();
	                    }
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	            outputDoCliente.close();
	            cliente.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
