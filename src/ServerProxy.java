import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerProxy {
	public static void main(String[] args) {
		try {
			Socket cliente;
			ServerSocket servidor = new ServerSocket(6000);
			while (true) {
				cliente = servidor.accept();
				BufferedReader in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
				String inputParcialDoCliente;
				StringBuffer inputInicialDoCliente = new StringBuffer();
				final String endLine = "\r\n";
				while ((inputParcialDoCliente = in.readLine()) != null) {
					if(inputParcialDoCliente.equals("")) break;
					inputInicialDoCliente.append(inputParcialDoCliente);
					inputInicialDoCliente.append(endLine);
				}
				inputInicialDoCliente.append(endLine);
				System.out.println(inputInicialDoCliente);
				int startOfUrl = inputInicialDoCliente.indexOf("Host: ") + "Host: ".length();
				int endOfUrl = inputInicialDoCliente.indexOf(endLine, startOfUrl);
				String urlDeDestino = inputInicialDoCliente.substring(startOfUrl, endOfUrl);
				if(urlDeDestino.equals("detectportal.firefox.com")) {
					cliente.close();
					continue;
				}
				new ProxyThread(cliente, InetAddress.getByName(urlDeDestino).getHostAddress(), inputInicialDoCliente.toString(), 80);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}